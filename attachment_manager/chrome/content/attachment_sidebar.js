var attachment_manager = {
    
    removeListeners: function() {
    	var el = document.getElementById("threadTree");
        el.removeEventListener("select", refreshPane, null);
        var cb = document.getElementById("attachment_sidebarframe").contentDocument.getElementById("selall");
        cb.removeEventListener("CheckboxStateChange", selectall, null);
    },
    
	//function to add event listeners to the checkbox and the thread tree.
    addListeners: function() {
    	setTimeout( function() {
        	var el = document.getElementById("threadTree");
            el.addEventListener("select", refreshPane, null);
        	var cb = document.getElementById("attachment_sidebarframe").contentDocument.getElementById("selall");
        	cb.addEventListener("CheckboxStateChange", selectall, null);
        	},500)
    }
};

//function to select all / deselect all the attachments in the list box.
function selectall() {
Application.console.log("message") 
    var listbox = document.getElementById("attachment_sidebarframe").contentDocument.getElementById("listbox");
    if (document.getElementById("attachment_sidebarframe").contentDocument.getElementById("selall").checked == true) {
        for (i = 0; i < listbox.getElementsByTagName("richlistitem").length; i++) {
            var item = listbox.getElementsByTagName("richlistitem")[i].getElementsByTagName("checkbox")[0];
            if (item.checked == false) {
                item.checked = true;
            }
        }
    } else {
        for (i = 0; i < listbox.getElementsByTagName("richlistitem").length; i++) {
            var item = listbox.getElementsByTagName("richlistitem")[i].getElementsByTagName("checkbox")[0];
            if (item.checked == true) {
                item.checked = false;
            }
        }
    }

}



function addaccount(){

    var name=prompt("The dropbox sync is done through a third party addon 'Sendtodropbox'.Please create account on this url http://sendtodropbox.com/.Please Enter the unique email address");
    if (name!=null && name!=""){
          var reg = new RegExp("^[0-9a-zA-Z_]+@sendtodropbox.com$");
          if(reg.test(name)){
          
            var store = Components.classes["@mozilla.org/storage/service;1"].
            getService(Components.interfaces.mozIStorageService);
            var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsILocalFile);
            // This makes file point to PROFILE_DIR/foo.sdb
            file.append("attachmentmngr.sqlite");
            var db = store.openDatabase(file);
            var sql = "INSERT INTO userTable(email,password) VALUES('"+name+"', '"+name+"')";
            db.executeSimpleSQL(sql);
           document.getElementById("dropboxsync").style.display="block";
           document.getElementById("dropboxx").style.display="none";
           document.getElementById("removedropbox").style.display="block";
            num_rows = 1;
            alert("Account Added!");
          }
          else
          {
            alert("Email Address Invalid!")
          }

        
      }


}
function removeaccount(){
    var store = Components.classes["@mozilla.org/storage/service;1"].
    getService(Components.interfaces.mozIStorageService);
    var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsILocalFile);
    // This makes file point to PROFILE_DIR/foo.sdb
    file.append("attachmentmngr.sqlite");
    var db = store.openDatabase(file);
    var sql = "DELETE FROM userTable";
    db.executeSimpleSQL(sql);
    document.getElementById("dropboxsync").style.display="none";
    document.getElementById("dropboxx").style.display="block";
    document.getElementById("removedropbox").style.display="none";
  //  document.getElementById("attachment_sidebarframe").contentDocument.getElementById("dropboxsync").style.display="none";
  //  document.getElementById("attachment_sidebarframe").contentDocument.getElementById("dropboxx").style.display="block";
    num_rows = 0;
    alert("Account removed!");

}


//function to refresh the list box display each time the email selection changes.
function refreshPane() {
    let msgHdrs = gFolderDisplay.selectedMessages;
    var i = 0;
    var params = new Array();
    
    for (i = 0; i < gFolderDisplay.selectedCount; i++) {
        MsgHdrToMimeMessage(msgHdrs[i], null, function (aMsgHdr, aMimeMessage) {
            let attachments = aMimeMessage.allAttachments;
            attachments = attachments.filter(function (x) x.isRealAttachment);
            params = params.concat(attachments);
            
        }, true);
    }
    
    
	/*a timeout is required because the above function requires 
	certain time to be able to retieve the params array.*/
    setTimeout(

    function () {
        
        var listbox = document.getElementById("attachment_sidebarframe").contentDocument.getElementById("listbox");
        document.getElementById("attachment_container").setAttribute("maxheight","10");
        document.getElementById("attachment_sidebarframe").style.display="none";
        var tooltip = clearList(listbox);
        
        var i = 0;
        for (i = 0; i < params.length; i++) {
            if(i==0){
            document.getElementById("attachment_container").setAttribute("maxheight","170");
            document.getElementById("attachment_sidebarframe").style.display="block";
                if(num_rows == 0){
                  //document.getElementById("attachment_sidebarframe").contentDocument.getElementById("dropboxsync").style.display="none";
                }
            }
            var item = createListItem(params[i], params[i].name, "moz-icon://" + params[i].name + "?size=40&contentType=" + params[i].contentType)
            listbox.appendChild(item);
            item.oncontextmenu = function(){
            var name=prompt("Enter the new name with extension",this.value.name);
            if (name!=null && name!=""){
                  this.value.name = name;
                  this.getElementsByTagName("label")[0].value = name;
                
              }
               
            };
            item.ondblclick = function(){
                var att = new Array();
                //alert(this.value);
                att.push(this.value);
                //alert(item.value.name);
                var path = getFilePath();
                saveAttachments(att, path);
                return true;
            };
        }
        if(params.length > 1)
        document.getElementById("attachment_header").value="Attachment View ("+params.length+" attachments)";
        else if(params.length == 1)
        document.getElementById("attachment_header").value="Attachment View ("+params.length+" attachment)";
        else document.getElementById("attachment_header").value="Attachment View";
    }, 1000)
}

//a function that creates a list box item for the attachment and returns it.
function createListItem(att, name, src) {
    var item = document.createElement("richlistitem");
    item.value = att;
    var check = document.createElement("checkbox");
    check.setAttribute("checked", false);
    var image = document.createElement("image");
    image.setAttribute("src", src);
    var label = document.createElement("label");
    label.setAttribute("value", name);

	//if it is an image attachment, enable preview by hover using the tooltip property.
    if (att.contentType.indexOf("image/") === 0) {
        var dialog = document.getElementById("attachment_sidebarframe").contentDocument.getElementById("attachmentsidebar");
        var tooltip = document.createElement("tooltip");
        tooltip.setAttribute("id", "tip" + name);
        var img = document.createElement("image");
        img.setAttribute("src", att.url);
        img.setAttribute("maxheight", "400");
        img.setAttribute("maxwidth", "400");
        image.setAttribute("src",att.url);
        image.setAttribute("maxheight", "40");
        image.setAttribute("maxwidth", "40");
        tooltip.appendChild(img);
        dialog.appendChild(tooltip);
        item.tooltip = tooltip.id;
    } else {
        //item.tooltipText = name;
    }
    item.appendChild(check);
    item.appendChild(image);
    item.appendChild(label);
    
    return item;
}

//function that is called when the forward button is clicked.
function onfor() {
    var attachments = getSelectedAttachments();
    composeMessageWithText(attachments);
    return true;
}

//function that is called when the download button is clicked.
function download() {
    var att = getSelectedAttachments();
    var path = getFilePath();
    saveAttachments(att, path);
    return true;
}

function onsync(){
    var attachments = getSelectedAttachments();
    sendAutoMsg(attachments);
    return true;

}
//function returns an array of attachments that were selected by the uses.
function getSelectedAttachments() {
    var listbox = document.getElementById("listbox");
    var attachments = new Array();
    for (i = 0; i < listbox.getElementsByTagName("richlistitem").length; i++) {
        if (listbox.getElementsByTagName("richlistitem")[i].getElementsByTagName("checkbox")[0].checked == true) {
            attachments.push(listbox.getElementsByTagName("richlistitem")[i].value);
        }
    }
    return attachments;
}

//function that open a file picker window and returns the path of the located chosen by the user.
function getFilePath() {
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, "Pick a Folder", nsIFilePicker.modeGetFolder);
    fp.appendFilters(nsIFilePicker.filterAll | nsIFilePicker.filterText);

    var fullfilepath;
    var rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
        var file = fp.file;
        // Get the path as string. 
        fullfilepath = fp.file.path;
    }
    return fullfilepath;
}

//function that saves attachments to a particular path in the computer.
function saveAttachments(att, fullfilepath) {
    let ioService = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
    messenger = Components.classes["@mozilla.org/messenger;1"].createInstance(Components.interfaces.nsIMessenger);

    for (i = 0; i < att.length; i++) {
        let neckoURL = null;
        neckoURL = ioService.newURI(att[i].url, null, null);
        neckoURL.QueryInterface(Components.interfaces.nsIMsgMessageUrl);
        let uri = neckoURL.uri;

        var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
        file.initWithPath(fullfilepath + "/" + att[i].name);
        if (!file.exists()) {
            file.create(0x00, 0644);
        }
        messenger.saveAttachmentToFile(file, att[i].url, uri, att[i].contentType, null);
    }
}

//function that resets the listbox to empty.
function clearList(listbox) {

    var times = listbox.getElementsByTagName("richlistitem").length;
   
   
    for (i = 0; i < times; i++) {
        element = listbox.getElementsByTagName("richlistitem")[0];
       
        element.parentNode.removeChild(element);
    }
}

function sendAutoMsg(att) {
    try {
         var msgComposeType = Components.interfaces.nsIMsgCompType;
         var msgComposFormat = Components.interfaces.nsIMsgCompFormat;
         var am = Components.classes["@mozilla.org/messenger/account-manager;1"].getService(Components.interfaces.nsIMsgAccountManager);
    
        var cf = Components.classes["@mozilla.org/messengercompose/composefields;1"].createInstance(Components.interfaces.nsIMsgCompFields);
        cf.from = "santom91@gmail.com";//need to add code to change this to users
        cf.to = "santom91@gmail.com;";//need to ad code to change
        /*
        have to test this code yet
        var account1 = am.defaultAccount;
         var identity1 = account1.defaultIdentity;
         
        var cf = Components.classes["@mozilla.org/messengercompose/composefields;1"].createInstance(Components.interfaces.nsIMsgCompFields);
        cf.from = identity1.email;
        cf.to = identity1.email;
        
        
        */
        var store = Components.classes["@mozilla.org/storage/service;1"].
        getService(Components.interfaces.mozIStorageService);
        var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsILocalFile);
        // This makes file point to PROFILE_DIR/foo.sdb
        file.append("attachmentmngr.sqlite");
        var db = store.openDatabase(file);
        var statement = Components.classes['@mozilla.org/storage/statement-wrapper;1'].createInstance(Components.interfaces.mozIStorageStatementWrapper);
        statement.initialize(db.createStatement("SELECT * FROM userTable"));
        if(statement.step())
        {
            cf.to += statement.row.email+";";
        }
        cf.subject = "attachment";
        cf.body = "";
        
        var params = Components.classes["@mozilla.org/messengercompose/composeparams;1"].createInstance(Components.interfaces.nsIMsgComposeParams);
         if (params) {
                params.type = msgComposeType.Template;
                params.format = msgComposFormat.Default;
                if (cf) {
                    params.composeFields = cf;
                    for (i = 0; i < att.length; i++) {
                        attachment = Components.classes["@mozilla.org/messengercompose/attachment;1"].createInstance(Components.interfaces.nsIMsgAttachment);
                        attachment.name = att[i].name;
                        attachment.url = att[i].url;
                        params.composeFields.addAttachment(attachment);
                    }

                }
            }
        params.smtpPassword = "v";
        var msgSend = Components.classes["@mozilla.org/messengercompose/send;1"].createInstance(Components.interfaces.nsIMsgSend);
        var msgCompose = Components.classes["@mozilla.org/messengercompose/compose;1"].createInstance(Components.interfaces.nsIMsgCompose);
        
        msgCompose.initialize(params); //passing null instead of window also doesnt work
        msgCompose.SendMsg(msgSend.nsMsgDeliverNow, am.defaultAccount.defaultIdentity, am.defaultAccount.key, null, null);
        alert("uploading files!!");
        }
    catch(ex)
    {
            alert("Debug Error: " + ex);
    }
}

//function that opens a new compose window with attachments pre-attached.
function composeMessageWithText(att) {
    var msgComposeType = Components.interfaces.nsIMsgCompType;
    var msgComposFormat = Components.interfaces.nsIMsgCompFormat;
    var msgComposeService = Components.classes['@mozilla.org/messengercompose;1'].getService();
    msgComposeService = msgComposeService.QueryInterface(Components.interfaces.nsIMsgComposeService);

    gAccountManager = Components.classes['@mozilla.org/messenger/account-manager;1'].getService(Components.interfaces.nsIMsgAccountManager);

    var params = Components.classes['@mozilla.org/messengercompose/composeparams;1'].createInstance(Components.interfaces.nsIMsgComposeParams);
    if (params) {
        params.type = msgComposeType.Template;
        params.format = msgComposFormat.Default;
        var composeFields = Components.classes['@mozilla.org/messengercompose/composefields;1'].createInstance(Components.interfaces.nsIMsgCompFields);
        if (composeFields) {
            params.composeFields = composeFields;
            for (i = 0; i < att.length; i++) {
                attachment = Components.classes["@mozilla.org/messengercompose/attachment;1"].createInstance(Components.interfaces.nsIMsgAttachment);
                attachment.name = att[i].name;
                attachment.url = att[i].url;
                params.composeFields.addAttachment(attachment);
            }

            msgComposeService.OpenComposeWindowWithParams(null, params);
        }
    }
}
